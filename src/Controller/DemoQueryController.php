<?php

namespace App\Controller;

use App\Entity\Project;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DemoQueryController extends AbstractController
{
    /**
     * @Route("/demo/query", name="demo_query")
     */
    public function index()
    {
        // most if not all the code of those methods should be part of a repo
        $this->queryFindOneBy();
        $this->queryFind();
        $this->queryFindMagic();
        $this->doctrineQueryLanguageObjects();
        $this->doctrineQueryLanguageArray();
        $this->doctrineQueryLanguageMix();
        $this->queryBuilder();
        $this->getCountProjects();
        $this->rawSQL();
        return $this->render('demo_query/index.html.twig', [
            'controller_name' => 'DemoQueryController',
        ]);
    }

    private function queryFindOneBy()
    {
        $projectRepository = $this->getDoctrine()
            ->getRepository(Project::class);
        $project = $projectRepository->findOneBy(['riskLevel' => 10]);
        echo "<p>Project Loaded Id {$project->getId()}</p>";
    }

    private function queryFind()
    {

        $projectRepository = $this->getDoctrine()
            ->getRepository(Project::class);
        $projects = $projectRepository->findBy(['riskLevel' => 10], ['id' => 'DESC'], 2, 1);

        foreach ($projects as $project) {
            echo "<p>Project Loaded Id {$project->getId()}</p>";
        }
    }


    private function queryFindMagic()
    {
        $projectRepository = $this->getDoctrine()
            ->getRepository(Project::class);
        $projects = $projectRepository->findByRiskLevel(10, ['id' => 'DESC'], 2, 1);

        foreach ($projects as $project) {
            echo "<p>Project Loaded Id {$project->getId()}</p>";
        }
    }

    private function doctrineQueryLanguageObjects()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = <<<DQL
        SELECT p FROM App\Entity\Project p WHERE p.riskLevel = 10
DQL;

        $query = $entityManager->createQuery($query);
        $query->setMaxResults(3);
        $projects = $query->getResult();

        foreach ($projects as $project) {
            echo "<p>Project Loaded Id {$project->getId()}</p>";
        }
    }

    private function doctrineQueryLanguageArray()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = <<<DQL
        SELECT p.id FROM App\Entity\Project p WHERE p.riskLevel = 10
DQL;

        $query = $entityManager->createQuery($query);
        $query->setMaxResults(3);
        $projectsArray = $query->getResult();

        foreach ($projectsArray as $project) {
            echo "<p>Project Loaded Id {$project['id']}</p>";
        }
    }

    private function doctrineQueryLanguageMix()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = <<<DQL
        SELECT q, p.id FROM App\Entity\Question q JOIN q.project p
DQL;

        $query = $entityManager->createQuery($query);
        $query->setMaxResults(3);
        $results = $query->getResult();

        foreach($results as $item) {
            echo "<p>Project Loaded Id {$item['id']} - Question Id {$item[0]->getId()}</p>";
        }
    }


    private function rawSQL()
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = <<<SQL
           SELECT
            *
           FROM project
          LIMIT 2
SQL;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $projectsArray = $stmt->fetchAll();

        foreach ($projectsArray as $project) {
            echo "<p>Project Loaded Id {$project['id']}</p>";
        }
    }

    private function queryBuilder()
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $queryBuilder->select('p')
            ->from(Project::class, 'p')
            ->where('p.riskLevel = ?1')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults(3);

        $queryBuilder->setParameter(1, 10);


        $query = $queryBuilder->getQuery();
        $projects = $query->getResult();

        foreach ($projects as $project) {
            echo "<p>Project Loaded Id {$project->getId()}</p>";
        }
    }

    private function getCountProjects()
    {
        $repository = $this->getDoctrine()->getRepository(Project::class);
        $countProjects = $repository->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();

        echo "<p>Total {$countProjects}</p>";
    }


}
