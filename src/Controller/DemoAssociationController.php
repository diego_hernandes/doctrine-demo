<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Question;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DemoAssociationController extends AbstractController
{
    /**
     * @Route("/demo/association", name="demo_association")
     */
    public function index()
    {
//        $this->addQuestionsToProjectNonCascade();
//        $this->addQuestionsToProjectWithCascade();
//        $this->addProjectToQuestion(); // This can be removed and force one way
//        $this->loadProjectWithQuestionsLazy(); //Project must have  fetch lazy
//        $this->loadProjectWithQuestionsEager(); //Project must have fetch eager
        $this->loadProjectWithQuestionsExtraLazy(); //Project must have fetch extra_lazy
        return $this->render('demo_association/index.html.twig', [
            'controller_name' => 'DemoAssociationController',
        ]);
    }

    private function addQuestionsToProjectNonCascade()
    {
        $project = new Project();
        $project->setRiskLevel(10);
        $project->setTitle('project title');

        $question1 = new Question();
        $question1->setDescription('description 1');

        $question2 = new Question();
        $question2->setDescription('description 2');

        $project->addQuestion($question1);
        $project->addQuestion($question2);

        $entityManager = $this->getDoctrine()->getManager();

        // the order of persist does not matter
        $entityManager->persist($question2);
        $entityManager->persist($project);
        $entityManager->persist($question1);

        $entityManager->flush();

        echo "<p>Project Added Id {$project->getId()}</p>";
        echo "<p>Question Added Id {$question1->getId()}</p>";
        echo "<p>Question Added Id {$question2->getId()}</p>";
    }

    private function addQuestionsToProjectWithCascade()
    {
        $project = new Project();
        $project->setRiskLevel(10);
        $project->setTitle('project title');

        $question1 = new Question();
        $question1->setDescription('description 1');
        $project->addQuestion($question1);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($project);
        $entityManager->flush();

        echo "<p>Project Added Id {$project->getId()}</p>";
        echo "<p>Question Added Id {$question1->getId()}</p>";
    }

    private function addProjectToQuestion()
    {
        $project = new Project();
        $project->setRiskLevel(5);
        $project->setTitle('project title');

        $question1 = new Question();
        $question1->setDescription('description 1');
        $project->addQuestion($question1);

        $question1->setProject($project);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($project);
        $entityManager->flush();

        echo "<p>Project Added Id {$project->getId()}</p>";
        echo "<p>Question Added Id {$question1->getId()}</p>";
    }

    private function loadProjectWithQuestionsLazy()
    {
        $projectRepository = $this->getDoctrine()->getRepository(Project::class);
        /** @var Project $project */
        $project1 = $projectRepository->find(1);
        $project2 = $projectRepository->find(2);

        $question1 = $project1->getQuestions()->get(1);

        echo "<p>Project Loaded Id {$project1->getId()}</p>";
        echo "<p>Project Loaded Id {$project2->getId()}</p>";
        echo "<p>Questiom Loaded Id {$question1->getId()}</p>";

        foreach ($project1->getQuestions() as $question) {
            echo "<p>Question Loaded Id {$question->getId()}</p>";
        }
    }

    private function loadProjectWithQuestionsEager()
    {
        $projectRepository = $this->getDoctrine()->getRepository(Project::class);
        /** @var Project $project */
        $project = $projectRepository->findOneBy([]);

        echo "<p>Project Loaded Id {$project->getId()}</p>";
    }

    private function loadProjectWithQuestionsExtraLazy()
    {
        $projectRepository = $this->getDoctrine()->getRepository(Project::class);
        /** @var Project $project */
        $project1 = $projectRepository->find(1);
        $project2 = $projectRepository->find(2);

        $question1 = $project1->getQuestions()->get(1);

        echo "<p>Project Loaded Id {$project1->getId()}</p>";
        echo "<p>Project Loaded Id {$project2->getId()}</p>";
        echo "<p>Project Loaded Id {$question1->getId()}</p>";

        foreach ($project1->getQuestions() as $question) {
            echo "<p>Question Loaded Id {$question->getId()}</p>";
        }
    }
}
