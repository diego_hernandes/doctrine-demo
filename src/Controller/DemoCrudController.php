<?php

namespace App\Controller;

use App\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DemoCrudController extends AbstractController
{
    /**
     * @Route("/demo/crud", name="demo_crud")
     */
    public function index()
    {

        $this->addCustomer();
        $this->listCustomers();
        $this->showCustomer();
        $this->updateCustomer();
        $this->deleteCustomer();
        $this->listCustomers();

        return $this->render('demo/index.html.twig', [
            'controller_name' => 'DemoController',
        ]);
    }

        private function addCustomer() {
        $customer = new Customer();
        $customer->setName(uniqid('Customer'));

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($customer);
        $entityManager->flush();

        echo "<p>Added - {$customer->getId()}</p>";
    }

    private function listCustomers() {
        $customerRepository = $this->getDoctrine()->getRepository(Customer::class);

        $customers = $customerRepository->findAll();

        foreach($customers as $customer) {
            echo "<p>{$customer->getId()}-{$customer->getName()}</p>";
        }
    }

    private function showCustomer() {
        $customerId = 3;
        $customerRepository = $this->getDoctrine()->getRepository(Customer::class);
        $customer = $customerRepository->find($customerId);
        echo "<p>Show One: {$customer->getId()}-{$customer->getName()}</p>";
    }

    private function updateCustomer() {
        $customerRepository = $this->getDoctrine()->getRepository(Customer::class);
        $customer = $customerRepository->findOneBy([]);

        $customer->setName(uniqid('Customer-Updated'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($customer);
        $entityManager->flush();

        echo "<p>Updated Id - {$customer->getId()}</p>";
    }


    private function deleteCustomer() {
        $customerRepository = $this->getDoctrine()->getRepository(Customer::class);
        $customers = $customerRepository->findBy([],['id'=> 'DESC'], 1);
        $customer = current($customers);


        echo "<p>Deleted Id - {$customer->getId()}</p>";

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($customer);
        $entityManager->flush();
    }

}
